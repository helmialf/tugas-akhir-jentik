import csv
import pandas as pd
import re

file = open('../localFINAL.ttl', 'w', encoding="utf-8")
#Define Prefixes
file.write("PREFIX exr: <http://example.org/resource/>\n")
file.write("PREFIX exp: <http://example.org/property/>\n")
file.write("PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n")
file.write("PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n")
file.write("PREFIX owl: <http://www.w3.org/2002/07/owl#>\n")
file.write("PREFIX foaf: <http://xmlns.com/foaf/0.1/>\n")
file.write("PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n")
file.write("PREFIX dbp: <http://dbpedia.org/property/>\n")
file.write("PREFIX dbo: <http://dbpedia.org/ontology/>\n")

#State Equivalencies
file.write(f"exp:webDomain owl:equivalentProperty foaf:term_homepage .\n")
file.write(f"exp:webDomain owl:equivalentProperty dbp:website .\n")
file.write(f"exp:locality owl:equivalentProperty dbp:location .\n")


df = pd.read_csv('data.csv')

for i, row in df.iterrows():

    if i == 10000:
        break
    try:
        iri = str(row.iloc[0]).replace(" ","_")
        iri = re.sub('[\W]+', '', iri)
        name = str(row.iloc[0]).replace("\"", '').replace("\'", '')
        # name = re.sub('[\W]+', '', name)

        print(i, end=" ")
    
        
        file.write(f"exr:{iri} dbp:name \"{name}\" ;\n")
        file.write(f"\texp:iriDirect \"{iri}\" ;\n")
        file.write(f"\texp:webDomain \"{row.iloc[1]}\" ;\n")
        file.write(f"\tdbo:foundingYear \"{row.iloc[2]}\" ;\n")
        file.write(f"\texp:industry \"{row.iloc[3]}\" ;\n")
        file.write(f"\texp:sizeRange \"{row.iloc[4]}\" ;\n")
        file.write(f"\texp:locality \"{row.iloc[5]}\" ;\n")
        file.write(f"\tdbp:locationCountry \"{row.iloc[6]}\"@en ;\n")
        file.write(f"\texp:linkedinURL \"{row.iloc[7]}\" ;\n")
        file.write(f"\texp:CurrEmployeeEst \"{row.iloc[8]}\" ;\n")
        file.write(f"\texp:TotalEmployeeEst \"{row.iloc[9]}\" .\n")



    except Exception as e:
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        print(e)
        print(i)
        print(row)
        print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")

        continue




            # file.write()
        
        # if i == 5:
        #     break


file.close()