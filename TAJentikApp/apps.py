from django.apps import AppConfig


class TajentikappConfig(AppConfig):
    name = 'TAJentikApp'
