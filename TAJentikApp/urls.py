from django.urls import path
from .views import *

urlpatterns = [
    path('',home, name='home'),
    path('resource/<str:iri>', resource_page),
]
