from django.shortcuts import render
from rdflib import Graph
from SPARQLWrapper import SPARQLWrapper
from difflib import SequenceMatcher

# Create your views here.
graph = Graph()
result = graph.parse('localFINAL.ttl')

prefix = """PREFIX exr: <http://example.org/resource/>
PREFIX exp: <http://example.org/property/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX dbp: <http://dbpedia.org/property/>
PREFIX dbo: <http://dbpedia.org/ontology/>"""

country_query = """ SELECT DISTINCT ?Country WHERE {[] dbp:locationCountry ?Country }"""
country_query_res = graph.query(country_query)
list_country = [str(i.Country) for i in country_query_res] 
list_country.remove('nan')

def home(request):
    context = {}

    context['list_country'] = list_country
    print("---")
    print(request)
    if request.method == "POST":
        if request.POST['query_type'] == "name":
            context['result'] = 'True'
            context['type'] = 'Name'
            context['value'] = request.POST['query_input_name']
            context['company_list'] = query_by_name(request.POST['query_input_name'])
        if request.POST['query_type'] == "country":
            context['result'] = 'True'
            context['type'] = 'Country'
            context['value'] = request.POST['query_input_country']
            context['company_list'] = query_by_country(request.POST['query_input_country'])
        print(len(context['company_list']))
            # context['company_list'] = query_by_name(request.POST['query_input'])
        # if request.POST['query_type'] == "country":
        #     context['company_list'] = query_by_country(request.POST['query_input'])

    return render(request, 'searchpage.html', context)

def resource_page(request, iri):
    print(iri)
    local = get_local_data(iri) #return 1

    dbpedia = query_dbpedia(local.CompanyName) # return 1
    context = {}

    context['CompanyName'] = dbpedia['Label']
    context['Abstract'] = dbpedia["Abstract"] #<<<<<<<<<<<
    context['Thumbnail'] = dbpedia['Thumbnail']
    context['AlternateName'] = dbpedia['AlternateName']
    context['WebDomain'] = local.WebDomain
    context['LinkedinURL'] = local.LinkedinURL
    context['FoundingYear'] = local.FoundingYear
    context['FoundedBy'] = dbpedia['FoundedBy']
    context['LocalIndustry'] = local.Industry
    context['Industries'] =  dbpedia['Industries']
    context['Locality'] = local.Locality
    context['LocationCountry'] = local.LocationCountry
    context['CurrEmployeeEst'] = local.CurrEmployeeEst
    context['TotalEmployeeEst'] = local.TotalEmployeeEst

    return render(request, 'resource.html', context)



def query_by_name(name):
    query = """SELECT DISTINCT ?CompanyName ?CompanyIRI 
        WHERE { 
            ?Company dbp:name ?CompanyName . 
            ?Company exp:iriDirect ?CompanyIRI . 
            FILTER( REGEX(?CompanyName, "%s", "i"))
        } 
        """ % name
    print(query)
    query_result = graph.query(query)
    
    # for row in query_result:

    return query_result


def query_by_country(country):
    query = """SELECT DISTINCT ?CompanyName ?CompanyIRI 
        WHERE { 
            ?Company dbp:locationCountry "%s"@en .
            ?Company dbp:name ?CompanyName .
            ?Company exp:iriDirect ?CompanyIRI .
        } 
        """ % country
    print(query)
    query_result = graph.query(query)
    
    # for row in query_result:

    return query_result

def get_local_data(iri):
    query = local_query(iri)
    query_result = graph.query(query)

    print(query)

    print(len(query_result))

    result = None
    for row in query_result:
        result = row
        break
    return result


def local_query(iri):
    return """SELECT DISTINCT ?CompanyName ?WebDomain ?FoundingYear ?Industry ?SizeRange ?Locality ?LocationCountry ?LinkedinURL ?CurrEmployeeEst ?TotalEmployeeEst
    WHERE {
        exr:%s dbp:name ?CompanyName ;
            exp:webDomain ?WebDomain ;
            dbo:foundingYear ?FoundingYear ;
            exp:industry ?Industry ;
            exp:sizeRange ?SizeRange ;
            exp:locality ?Locality ;
            dbp:locationCountry ?LocationCountry ;
            exp:linkedinURL ?LinkedinURL ;
            exp:CurrEmployeeEst ?CurrEmployee ;
            exp:TotalEmployeeEst ?TotalEmployee .

            BIND(STR(?CurrEmployee) AS ?CurrEmployeeEst)
            BIND(STR(?TotalEmployee) AS ?TotalEmployeeEst)

    }
    """ % iri


def query_dbpedia(name):
    query = prefix + tight_query(name)
    print(query)
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setQuery(query)
    sparql.setReturnFormat("json")
    results = sparql.query().convert()


    if len(results["results"]["bindings"]) == 0:
        query = prefix + loose_end_query(name) # karena ada yg ditambah "Corp dibelakangnya"
        print('Masuk Loose End')
        print(query)
        sparql = SPARQLWrapper("http://dbpedia.org/sparql")
        sparql.setQuery(query)
        sparql.setReturnFormat("json")
        results = sparql.query().convert()


    print(results["results"]["bindings"])

    if len(results["results"]["bindings"]) > 1:
        print("MASUK GET MOST SIMILAR")
        entity = get_most_similar(name, results["results"]["bindings"])
        print(len(entity))
        print(type(entity))
    else:
        entity = results["results"]["bindings"][0]


    query_entity_detail = get_detail(entity['Company']['value'])
    print(len(query_entity_detail))
    print(query_entity_detail[0])
    entity_detail = {}
    KEYS = query_entity_detail[0].keys()


    entity_detail['Label'] = results["results"]["bindings"][0]["Label"]['value']
    entity_detail['Abstract'] = query_entity_detail[0]['Abstract']['value']
    if 'Name' not in query_entity_detail[0].keys() or 'Label' not in query_entity_detail[0].keys():
        entity_detail['AlternateName'] = 'Not Found'
    else:
        if results["results"]["bindings"][0]["Label"]['value'] != query_entity_detail[0]['Name']['value']:
            entity_detail['AlternateName'] = query_entity_detail[0]['Name']['value']
        else:
            entity_detail['AlternateName'] = query_entity_detail[0]['Label']['value']
    if 'Thumbnail' in query_entity_detail[0].keys():
        entity_detail['Thumbnail'] = query_entity_detail[0]['Thumbnail']['value'].replace('wiki-commons:', 'http://commons.wikimedia.org/wiki/')
    else:
        entity_detail['Thumbnail'] = "https://cdn-a.william-reed.com/var/wrbm_gb_food_pharma/storage/images/9/2/8/5/235829-6-eng-GB/Feed-Test-SIC-Feed-20142_news_large.jpg"
    
    if 'FoundedBy' in KEYS:
        entity_detail['FoundedBy'] = query_entity_detail[0]['FoundedBy']['value']
        founders = set()
        for i in range(len(query_entity_detail)):
            founders.add(query_entity_detail[i]["FoundedBy"]['value'])
        entity_detail['FoundedBy'] = list(founders)
    else:
        entity_detail['FoundedBy'] = ['Not Found']
    industries = set()
    for i in range(len(query_entity_detail)):
        industries.add(query_entity_detail[i]["Industry_str"]['value'])
    entity_detail['Industries'] = list(industries)

    #Abstract
    #FoundedBy
    #Industry
    #foaf:name
    return entity_detail


def get_most_similar(target_name, list_result_query):
    most_similar = ''
    similarity_holder = 0

    for ent in list_result_query:
        label = ent['Label']['value']

        score = similarity(target_name, label)

        if score > similarity_holder:
            similarity_holder = score
            most_similar = ent

    return most_similar

def similarity(a,b):
    return SequenceMatcher(None, a, b).ratio()

def get_detail(uri):
    query = prefix + detail_query(uri)
    print(query)
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setQuery(query)
    sparql.setReturnFormat("json")
    results = sparql.query().convert()

    return results["results"]["bindings"]


def detail_query(uri):
    return '''\nSELECT DISTINCT ?Abstract ?Name ?Label ?Thumbnail ?FoundedBy ?Industry_str Where {
            <'''+uri+'''> dbo:abstract ?Abstract .
            FILTER(lang(?Abstract) = 'en')
            OPTIONAL {
                <'''+uri+'''> rdfs:label ?Label .
                FILTER(lang(?Label) = 'en')
            }
            OPTIONAL {
                <'''+uri+'''> foaf:name ?Name . 
                FILTER(lang(?Name) = 'en')
            }

            OPTIONAL {
                <'''+uri+'''> dbo:thumbnail ?Thumbnail . 
            }

            OPTIONAL {
                <'''+uri+'''> dbo:foundedBy ?FoundedByEnt .
                BIND(STR(?FoundedByEnt) AS ?FoundedBy)
            }

            OPTIONAL {
                <'''+uri+'''> dbo:industry ?Industries . 
                BIND(STR(?Industries) AS ?Industry_str)
            }

        }
        '''



def tight_query(name):
    return '''\nSELECT DISTINCT ?Company ?Label Where {
            ?Company rdf:type dbo:Company .

            {
                ?Company rdfs:label ?Label .
                FILTER(lang(?Label) = 'en')
                FILTER(REGEX(?Label, "^'''+name+'''$", "i"))
            } UNION {
                ?Company foaf:name ?Label .
                FILTER(lang(?Label) = 'en')
                FILTER(REGEX(?Label, "^'''+name+'''$", "i"))
            }
        }
        '''

def loose_end_query(name):
    return '''\nSELECT DISTINCT ?Company ?Label Where {
            ?Company rdf:type dbo:Company .

            {
                ?Company rdfs:label ?Label .
                FILTER(lang(?Label) = 'en')
                FILTER(REGEX(?Label, "^'''+name+'''", "i"))
            } UNION {
                ?Company foaf:name ?Label .
                FILTER(lang(?Label) = 'en')
                FILTER(REGEX(?Label, "^'''+name+'''", "i"))
            }
        }
        '''
